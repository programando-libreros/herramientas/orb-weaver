module OrbWeaver
  class Pack
    def initialize
      $pre_ast[:packers].each do |packer|
        key = packer.keys.first
        obj = key.to_s.split('_').map{|e| e.capitalize}.join('')
        packer = packer[key]
        OrbWeaver::Pack.const_get(obj).new(key.to_s + '/')
      end
    end

    # TODO: $pre_ast => $ast
  end
end
