require 'securerandom'

module OrbWeaver
  class Pack
    class Website
      @@deployer = OrbWeaver::Deployers.new

      # TODO: refactor this shit

      def initialize dir_root
        prt('packer_website_ini')
        dir_html = 'html/'
        mkdir($out)
        mkdir($out + dir_root, true)
        mkdir($out + dir_root + dir_html)
        @@deployer.assets($out + dir_root)
        deploy_html(dir_root)
        prt('packer_website_end')
      end

      def deploy_html dir_packer
        dir_html = $out + dir_packer + 'html/'
        @dir_presets = $root + $presets + dir_packer
        $pre_ast[:canvases].each do |html|
          @h = html
          @file_name = @h[:metas].reject{|x| !x[:id]}
          @file_name = @file_name[0][:id][0] + '.html'
          preset = File.read(@dir_presets + 'canvas.html')
          preset.gsub!(':content[]', @h[:frame].join(''))
          put_metas(preset)
          save(dir_html + @file_name, preset)
        end
        put_index(dir_packer, dir_html)
      end

      def put_index dir_packer, dir_html
        index = File.read(@dir_presets + 'index.html')
        index_canvas = $pre_ast[:packers][0][:website][:index] + '.html'
        index.gsub!(':index[]', 'html/' + index_canvas)
        save($out + dir_packer + 'index.html', index)
      end
 
      def put_metas preset
        @h[:metas].each do |m|
          key = ':' + m.keys.first.to_s
          case key
          when ':scripts', ':styles'
            val = put_script_style(m, key)
          else val = m.values.first.join(',')
          end
          preset.gsub!(/#{key}\[*\]*/, val) 
        end
      end

      def put_script_style m, key
        val = []
        ver = SecureRandom.hex(2)
        m.values.first.each do |v|
          if key == ':scripts'
            val.push('<script type="application/javascript" src="../' + $assets + v + '?v=' + ver  + '"></script>')
          else
            val.push('<link type="text/css" rel="stylesheet" href="../' + $assets + v + '?v=' + ver  + '" />')
          end
        end
        val = val.join("\n")
      end
    end
  end
end
