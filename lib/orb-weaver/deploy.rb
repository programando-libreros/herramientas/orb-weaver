require 'vine'

module OrbWeaver
  class Deploy
    @@pars = ['debug', 'production']
    @@deploy = OrbWeaver::Deployers.new

    def initialize type = nil 
      @par  = type ? 'debug' : verify_pars(@@pars)
      verify_project
      verify_canvases
      prt('to_' + @par, {:replace => @name})
      recipes = {:project => init_project, :canvases => init_canvases}
      match_metas(recipes)
      $pre_ast = @@deploy.frame(recipes)
      send('to_' + @par)
      OrbWeaver::Pack.new
    end

    # TODO: it should init as class
    def init_project
      path = Dir.pwd + '/' + Dir.children(Dir.pwd).reject{|f| File.extname(f) != '.rb'}.first
      require path
      $project
    end

    # TODO: it should init as class
    def init_canvases
      canvases = verify_canvases
      $canvases = []
      # WARN: each recipe push to $canvases and each recipe is load by require
      canvases.map{|f| require(Dir.pwd + '/' + f)}
      $canvases
    end

=begin
    Matches metadata as follows:
      project == isolated project recipe meta
      canvas  == isolated canvas recipe meta
      same    == equal meta values between project and canvas
      diff1   == uniq meta values of project
      diff2   == uniq meta values of canvas
      diff3   == rejects keys from diff1 hash that are present in diff2 hash,
                 so the result is meta values of project whose keys aren't present in canvas;
                 == those meta key and values of project that canvas doesn't have
      final   == concat of same, diff2 and diff3 converted to a hash 
=end
    def match_metas recipes
      project = recipes[:project][:metas].to_a
      recipes[:canvases].each_with_index do |recipe, i|
        canvas = recipe[:metas].to_a
        same   = canvas & project
        diff1  = project - canvas
        diff2  = canvas - project
        diff3  = diff1.to_h.reject{|k,v| diff2.to_h.has_key?(k)}.to_a
        final  = (same + diff2 + diff3).map{|m| m.map{|e| e.class == String ? [e] : e}}
        recipes[:canvases][i].delete(:metas)
        recipes[:canvases][i][:metas] = final
        recipes[:canvases][i][:metas].map!{|e| {e[0] => e[1]}}
      end
      recipes[:project].delete(:metas)
      recipes[:packers] = recipes[:project].access('packers')
      recipes.delete(:project)
    end

    def to_debug
      $minify = false
    end

    def to_production
      $minify = true
    end
  end
end
