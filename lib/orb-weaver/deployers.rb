module OrbWeaver
  class Deployers

    def initialize; end

    def frame recipes
      @deps = {}
      @recipes = recipes
      @recipes[:assets] = []
      @recipes[:canvases].each do |canvas|
        @canvas = canvas
        canvas[:frame].map!{|e| verify_asset_type(e)}
      end
      include_assets_from_snippets
      include_assets
      @recipes
    end

    def preset
      tag = @a[:preset].split(/\s+/).first
      content = "<#{tag}>:content[]</#{tag}>"
      var(content)
    end

    def snippet
      path_asset = get_path(@a[:snippet])
      file = verify_file(path_asset)
      content = File.read(file)
      var(content)
    end

    def var content
      verify_asset
      user = @a[:var]
      auto = get_default_opt(content)
      verify_opt(user, auto)  
      auto.reject!{|k, v| auto.has_key?(k)}
      user.merge!(auto)
      if !user.empty? then verify_var(user) end
      content(content, user)
    end

    def content content, var
      var.each do |k, v|
        opt = ':' + k.to_s
        rgx = /^\S+\.\S+$/
        cnt = v.first =~ rgx ? get_cnt(v.first) : v.first
        content.gsub!(/#{opt}\[[^\]]*\]/, cnt)
        v.shift
      end
      content.gsub!(/:\S+\[([^\]]*)\]/, '\1')
      content
    end

    def assets root
      root += $assets
      mkdir(root)
      $pre_ast[:assets].each do |asset|
        if !$minify
          FileUtils.cp_r($assets + asset, root)
        else
          # TODO: group by type & minify
        end
      end
    end

private

    def get_default_opt content
      default = {}
      regex = /\:(\w+)\[([^\]]*)\]((?:{[^}]+?}))*/
      match = content.scan(regex).each do |m|
        opt = m[0]; val = m[1]; dep = m[2]
        default[opt.to_sym] = val
        if dep
          content.gsub!(dep, '')
          deps = dep[1..-2].split(',').map{|d| d.strip}
          add_deps(deps)
        end
      end
      default
    end

    def get_cnt cnt
      file = get_path(cnt)
      verify_cnt([file], cnt.to_s)
      if text?(file) then File.read(file)
      else
        @recipes[:assets] += [file.gsub($assets, '')]
        File.basename(file)
      end
    end

    def get_path file
      Dir.glob($assets + '**/*.*')
         .reject{|f| File.basename(f) != file}
         .first
    end

    def add_deps deps
      s = @a[:snippet]
      if !@deps[s] then @deps[s] = [] end
      @deps[s] += deps
      @deps[s].uniq!
    end

    def include_assets_from_snippets
      @deps.each do |k, v|
        prt('warn_deps1', {:replace => k})
        prt('warn_deps2', {:replace => v.join(', ')})
        @recipes[:assets] += v
      end
    end

    def include_assets
      assets = []

      @recipes[:packers].each do |packer|
        name  = packer.keys.first
        if packer[name][:assets] then assets += packer[name][:assets] end
        packer[name].delete(:assets)
      end

      @recipes[:canvases].each do |recipe|
        recipe[:metas].each do |meta|
          key = meta.keys.first
          if key == :scripts || key == :styles || key == :favicon || key == :assets
            assets += meta[key]
          end
        end
      end

      @recipes[:assets] += assets
      @recipes[:assets].reject!{|a| a.empty?}
      verify_assets
    end

    def verify_assets
      @recipes[:assets].map{|a| verify_file($assets + a)}
      prt('warn_deps3')
    end

    def verify_asset_type asset
      @a = asset
      case @a.class.to_s
      when 'Hash'
        case @a.keys.first
        when :preset  then preset
        when :snippet then snippet
        else prt_err('asset2')
        end
      when 'String' then @a
      else prt_err('asset1')
      end
    end

    def verify_asset
      v1 = @a[:var] ? @a[:var] : false
      v2 = v1 && v1.class == Hash ? true : false
      err?(v1, v2, 'var')
    end

    def verify_var var = @a
      v1 = var.values.length > 0 ? var.values.reject{|e| e.class == Array} : false
      v2 = v1 && v1.length == 0 ? true : false
      err?(v1, v2, 'opt')
    end

    def verify_opt user, auto
      user = user.map{|k,v| k}
      auto = auto.map{|k,v| k}
      if (user - auto).length > 0
        prt_err('opt3')
      end
    end

    def verify_cnt files, cnt
      if files.length > 1
        prt('error_cnt1', {:replace => cnt})
        files.map{|f| prt('error_cnt2', {:replace => f})}
        abort
      end
    end

    def err? v1, v2, type
      r  = v1 && v2
      if !r
        err = !v1 ? 1 : 2
        prt_err(type + err.to_s, @a)
      end
    end
  end
end
