> **UNDER DEVELOPMENT**: more packers are coming!

# orb-weaver

Abstract Syntax Tree deployer. Work with assets and let the spider weave AST, catch, wrap and liquify code for websites, apps, games and epublications.

## Docs

* [Español](./locales/help.es.md)

## Installation

### For production

```
gem install orb-weaver
```

### For development

Add the following to Gemfile and then execute bundle:

```ruby
gem 'borrar'
```

```
bundle install
```

### For testing

``` 
bin/setup && bundle exec rake install && gem install pkg/*.gem
```

## Contributing

> Check the [Todo list](TODO.md)

Bug reports and pull requests are welcome on [GitLab](https://gitlab.com/programando-libreros/herramientas/orb-weaver).

## Acknowledgements

> TODO: add links and translate.

* Programando LIBREros:
  * Por arriesgarse en otros modos de organización en tiempos de crisis global.
  * Melisa: por el diálogo y la reflexión sobre __lo que hacemos__.
  * Origen: organizaciones y proyectos fallidos.
* Miau:
  * Por mostrar que otros mundos, infraestructuras, tecnologías y metodologías son posibles.
  * Hacklib: por la introducción a lo que llama «hackedición».
  * Origen: trabajo en Rancho Electrónico y Colima Hacklab.
* Orbilibro:
  * Por apoyar las posturas políticas y de trabajo más extrañas para la industria editorial en México.
  * Julián: por creer en que nuestro trabajo puede ayudar a la defensa ante la crisis editorial en México.
  * Origen: publicación de entradas en el _blog_ de Mariana Eguaras.
* PIP:
  * Por la noción de web estática.
  * Por la noción de web autocontenida.
  * Fauno: por motivar el uso de Ruby y el desarrollo de gemas.
  * Origen: trabajo de Sutty, como el desarrollo de plugins de Jekyll.
* Grafoscopio:
  * Por el concepto de práctica POSSE.
  * Por el concepto de plantillas logic-less.
  * Offray: por la exposición de un mar de información.
  * Origen: reuniones Indie Web y ejemplo de herramientas y metodologías usando Pharo.
* LIDSOL:
  * Por el fomento a la tecnificación en el uso de _software_ libre.
  * Por la exposición a temas de GNU/Linux.
  * Gunnar: por dar ánimos de continuar trabajando sin importar las limitaciones técnicas.
  * Origen: publicación de sus materiales de docencia y consultas.
* Ibero:
  * Por el financiamiento al trabajo de Programando LIBREros.
  * Gabi: por confiar en nuestro trabajo.
  * Origen: seminario interdisciplinario truncado sobre Filosofía de la práctica editorial.
* Academia Mexicana de la Lengua:
  * Por el financiamiento al trabajo de Programando LIBREros.
  * Edgar y Pedro: por confiar en nuestro trabajo.
  * Origen: taller de edición digital a partir de un encuentro en FFyL-UNAM.

Este trabajo es una muestra de que la reforma a la Ley Federal de Derecho de Autor del Estado en México es errada: la cultura __es más__ que su apropiación.

## License

The gem is available as free software under the terms of the
[GNU General Public License version 3](https://opensource.org/licenses/GPL-3.0).

## Contact

Do you need more support? Email us at [hi@programando.li](mailto:hi@programando.li) :)
